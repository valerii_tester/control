package com.testapp.ui.data;

import org.testng.annotations.DataProvider;

public class UserDataProvider {

  @DataProvider(name = "userLoginData", parallel = true)
  public static Object[][] userData() {
    return new Object[][] {
      {"admin@test.com", "qwerty1"},
      {"j.accountant@test.com", "qwerty2"},
      {"accountant@test.com", "qwerty3"},
      {"s.accountant@test.com", "qwerty4"},
      {"manager@test.com", "qwerty5"}
    };
  }
}
