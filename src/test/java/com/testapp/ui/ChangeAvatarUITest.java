package com.testapp.ui;

import com.google.common.collect.ImmutableMap;
import com.testapp.ui.base.BaseUITest;
import com.testapp.ui.data.UserDataProvider;
import com.testapp.ui.pageobject.LoginPage;
import com.testapp.util.PropertyCollector;
import io.qameta.allure.Description;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static com.testapp.util.PropertyInitializer.UPLOAD_USER_AVATAR_MESSAGE;
import static com.testapp.util.PropertyInitializer.USER_AVATAR_LOCATION;

public class ChangeAvatarUITest extends BaseUITest {

  private LoginPage loginPage = new LoginPage();
  private ImmutableMap<String, String> loginData;
  private PropertyCollector propertyCollector;

  @Factory(dataProvider = "userLoginData", dataProviderClass = UserDataProvider.class)
  public ChangeAvatarUITest(String email, String password) {
    loginData =
        ImmutableMap.<String, String>builder()
            .put("email", email)
            .put("password", password)
            .build();
  }

  @Test(testName = "Upload user avatar.")
  @Description("Verify that user have an opportunely to upload his avatar.")
  void changeUserAvatar() throws FileNotFoundException {
    loginPage
        .enterEmail(loginData.get("email"))
        .enterPassword(loginData.get("password"))
        .clickLoginButton()
        .openProfilePage()
        .clickOnChangeUserAvatarButton()
        .uploadUserAvatar(USER_AVATAR_LOCATION)
        .getUploadedUserAvatarMessage(UPLOAD_USER_AVATAR_MESSAGE);
  }
}
