package com.testapp.ui.base;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.codeborne.selenide.Selenide.*;
import static com.testapp.util.PropertyInitializer.LOGIN_PAGE_URL;
import static com.testapp.util.PropertyInitializer.SELENOID_HUB_URL;

public class BaseUITest {

  @BeforeClass()
  public void setUp() {
    SelenideLogger.addListener(
        "AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
    Configuration.startMaximized = true;
    Configuration.remote = SELENOID_HUB_URL;
    Configuration.browserCapabilities.setCapability("enableVNC", true);
    open(LOGIN_PAGE_URL);
  }

  @AfterClass
  public void tearDown() {
    Selenide.closeWebDriver();
  }
}
