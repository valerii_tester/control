package com.testapp.api;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.testapp.api.assertion.PermissionAssertion;
import com.testapp.api.base.BaseAPITest;
import com.testapp.api.data.PermissionsDataProvider;
import com.testapp.api.model.LoginModel;
import io.qameta.allure.Description;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

public class LoginTest extends BaseAPITest {

  private ImmutableMap<String, String> loginData;

  @Factory(dataProvider = "userData", dataProviderClass = PermissionsDataProvider.class)
  public LoginTest(String email, String password, ImmutableList permissionsList) {
    loginData =
        ImmutableMap.<String, String>builder()
            .put("email", email)
            .put("password", password)
            .put("permissions", permissionsList.toString())
            .build();
  }

  @Test(testName = "Get permission.")
  @Description("Verify that each user have correct permissions.")
  void getPermissions() {
    LoginModel loginModel = new LoginModel().withLogin(loginData.get("email")).withPassword(loginData.get("password"));
    userController.getUserPermission(tokenController.getToken(loginModel));
    PermissionAssertion.verifyPermission(tokenController.getToken(loginModel), loginData.get("permissions"));
  }
}
