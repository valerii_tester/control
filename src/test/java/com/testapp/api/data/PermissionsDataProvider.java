package com.testapp.api.data;

import com.google.common.collect.ImmutableList;
import org.testng.annotations.DataProvider;

public class PermissionsDataProvider {

  @DataProvider(name = "loginData", parallel = true)
  public static Object[][] userData() {
    return new Object[][] {
      {"admin@test.com", "qwerty1", ImmutableList.of("READ", "CREATE", "UPDATE", "DELETE", "BULK_CREATE")},
      {"j.accountant@test.com", "qwerty2", ImmutableList.of("READ")},
      {"accountant@test.com", "qwerty3", ImmutableList.of("READ", "CREATE")},
      {"s.accountant@test.com", "qwerty4", ImmutableList.of("READ", "CREATE", "UPDATE")},
      {"manager@test.com", "qwerty5", "READ", ImmutableList.of("CREATE", "UPDATE", "BULK_CREATE")}
    };
  }
}
