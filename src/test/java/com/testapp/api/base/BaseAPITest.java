package com.testapp.api.base;

import com.testapp.api.controller.TokenController;
import com.testapp.api.controller.UserController;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;

import static com.testapp.util.PropertyInitializer.BASE_URI;

public class BaseAPITest {

  protected TokenController tokenController = new TokenController();
  protected UserController userController = new UserController();

  @BeforeClass
  public void config() {
    RequestSpecification specification =
        new RequestSpecBuilder()
            .setBaseUri(BASE_URI)
            .setBasePath("/")
            .addHeader("grant_type", "client_credentials")
            .setContentType(ContentType.JSON)
            .addFilter(new ResponseLoggingFilter())
            .build();
    RestAssured.requestSpecification = specification;
  }
}
