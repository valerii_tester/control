package com.testapp.api.constant;

public final class EndPoint {

  public static final String TOKEN = "token";
  public static final String PROFILE = "profile";

  private EndPoint() {
    throw new UnsupportedOperationException("This is a utility class and cannot be instantiated!");
  }
}
