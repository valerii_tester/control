package com.testapp.api.controller;

import com.testapp.api.model.LoginModel;
import com.testapp.api.model.TokenModel;
import org.apache.http.HttpStatus;

import static com.testapp.api.constant.EndPoint.TOKEN;
import static io.restassured.RestAssured.given;

public class TokenController {

  public TokenModel getToken(LoginModel loginModel) {
    return given()
        .body(loginModel)
        .log()
        .uri()
        .log()
        .body()
        .when()
        .post(TOKEN)
        .then()
        .log()
        .body()
        .assertThat()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .body()
        .as(TokenModel.class);
  }
}
