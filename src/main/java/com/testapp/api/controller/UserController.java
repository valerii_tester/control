package com.testapp.api.controller;

import com.testapp.api.model.TokenModel;
import com.testapp.api.model.UserModel;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;

import static com.testapp.api.constant.EndPoint.PROFILE;
import static io.restassured.RestAssured.given;

public class UserController {

  public UserModel getUserPermission(TokenModel tokenModel) {

    RequestSpecification spec =
        new RequestSpecBuilder()
            .addHeader("Authorization", tokenModel.tokenType + tokenModel.accessToken)
            .build();

    return given()
        .spec(spec)
        .when()
        .get(PROFILE)
        .then()
        .assertThat()
        .statusCode(HttpStatus.SC_OK)
        .and()
        .extract()
        .body()
        .as(UserModel.class);
  }
}
