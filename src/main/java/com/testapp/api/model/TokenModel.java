package com.testapp.api.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"access_token", "expires_in", "token_type"})
public class TokenModel implements Serializable {

  @JsonProperty("access_token")
  public String accessToken;

  @JsonProperty("expires_in")
  public int expiresIn;

  @JsonProperty("token_type")
  public String tokenType;

  @JsonProperty("permission")
  public PermissionModel permission;

  public TokenModel() {}

  /**
   * @param expiresIn
   * @param accessToken
   * @param tokenType
   * @param permission
   */
  public TokenModel(String accessToken, int expiresIn, String tokenType, PermissionModel permission) {
    super();
    this.accessToken = accessToken;
    this.expiresIn = expiresIn;
    this.tokenType = tokenType;
    this.permission = permission;
  }

  public TokenModel withAccessToken(String accessToken) {
    this.accessToken = accessToken;
    return this;
  }

  public TokenModel withExpiresIn(int expiresIn) {
    this.expiresIn = expiresIn;
    return this;
  }

  public TokenModel withTokenType(String tokenType) {
    this.tokenType = tokenType;
    return this;
  }

  public TokenModel withPermission(PermissionModel permission) {
    this.permission = permission;
    return this;
  }

}
