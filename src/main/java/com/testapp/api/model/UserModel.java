package com.testapp.api.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"firstName", "lastName", "email", "age", "profilePicture", "userRole"})
public class UserModel implements Serializable {

  @JsonProperty("firstName")
  public String firstName;

  @JsonProperty("lastName")
  public String lastName;

  @JsonProperty("email")
  public String email;

  @JsonProperty("age")
  public int age;

  @JsonProperty("profilePicture")
  public String profilePicture;

  @JsonProperty("userRole")
  public String userRole;

  public UserModel() {}

  /**
   * @param firstName
   * @param lastName
   * @param profilePicture
   * @param userRole
   * @param email
   * @param age
   */
  public UserModel(
      String firstName,
      String lastName,
      String email,
      int age,
      String profilePicture,
      String userRole) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.age = age;
    this.profilePicture = profilePicture;
    this.userRole = userRole;
  }

  public UserModel withFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public UserModel withLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public UserModel withEmail(String email) {
    this.email = email;
    return this;
  }

  public UserModel withAge(int age) {
    this.age = age;
    return this;
  }

  public UserModel withProfilePicture(String profilePicture) {
    this.profilePicture = profilePicture;
    return this;
  }

  public UserModel withUserRole(String userRole) {
    this.userRole = userRole;
    return this;
  }
}
