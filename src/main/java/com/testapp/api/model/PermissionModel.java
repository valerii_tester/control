package com.testapp.api.model;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"permission"})
public class PermissionModel implements Serializable {

  @JsonProperty("permission")
  public String permission;

  public PermissionModel() {}

  /** @param permission */
  public PermissionModel(String permission) {
    super();
    this.permission = permission;
  }

  public PermissionModel withPermission(String permission) {
    this.permission = permission;
    return this;
  }
}
