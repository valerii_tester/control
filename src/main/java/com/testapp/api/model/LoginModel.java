package com.testapp.api.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"login", "password"})
public class LoginModel implements Serializable {

  @JsonProperty("login")
  public String login;

  @JsonProperty("password")
  public String password;

  public LoginModel() {}

  /**
   * @param password
   * @param login
   */
  public LoginModel(String login, String password) {
    super();
    this.login = login;
    this.password = password;
  }

  public LoginModel withLogin(String login) {
    this.login = login;
    return this;
  }

  public LoginModel withPassword(String password) {
    this.password = password;
    return this;
  }
}
