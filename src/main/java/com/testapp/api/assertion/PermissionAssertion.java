package com.testapp.api.assertion;

import com.testapp.api.model.TokenModel;
import org.testng.asserts.SoftAssert;

public class PermissionAssertion {

  private static SoftAssert softAssert = new SoftAssert();

  public static void verifyPermission(TokenModel tokenModel, String permissionList) {
    {
      softAssert.assertEquals(
          tokenModel.permission.permission, permissionList, "Can't get user permission!");
    }
    softAssert.assertAll();
  }
}
