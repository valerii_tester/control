package com.testapp.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyCollector {

  private Properties property;

  public PropertyCollector(String filePath) {
    property = new Properties();
    try (FileInputStream stream = new FileInputStream(filePath)) {
      property.load(stream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public String getProperty(String key) {
    return property.getProperty(key);
  }
}
