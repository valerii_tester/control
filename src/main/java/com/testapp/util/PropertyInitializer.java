package com.testapp.util;

import java.io.File;

public final class PropertyInitializer {

  private static String pathToSystemProps = "src/main/resources/ui/system.properties";
  private static String pathToUrlProps = "src/main/resources/ui/url.properties";
  private static String pathToUserProps = "src/main/resources/ui/userresources.properties";

  public static final String LOGIN_PAGE_URL = new PropertyCollector(pathToUrlProps).getProperty("login.page.url");
  public static final String SELENOID_HUB_URL = new PropertyCollector(pathToUrlProps).getProperty("selenoid.hub");
  public static final String UPLOAD_USER_AVATAR_MESSAGE = new PropertyCollector(pathToSystemProps).getProperty("uploaded.avatar.message");
  public static final File USER_AVATAR_LOCATION = new File(new PropertyCollector(pathToUserProps).getProperty("user.avatar.location"));


  private static final String pathToApi = "src/main/resources/api/url.properties";

  public static final String BASE_URI = new PropertyCollector(pathToApi).getProperty("base.uri");


  private PropertyInitializer() {
    throw new UnsupportedOperationException("This is a utility class and cannot be instantiated!");
  }
}
