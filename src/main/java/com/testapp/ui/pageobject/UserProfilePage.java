package com.testapp.ui.pageobject;

import io.qameta.allure.Step;

import java.io.File;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$x;

public class UserProfilePage {

  private String uploadUserAvaterButtonLocator = "//div[@class='foo']";
  private String changeUserAvatarButtonLocator =  "//div[@class='foo']";
  private String uploadedUserAvatarMessageLocator = "//div[@class='foo']";

  @Step("Click the \"Change User Avatar Button\".")
  public UserProfilePage clickOnChangeUserAvatarButton() {
    $x(changeUserAvatarButtonLocator).click();
    return this;
  }

  @Step("Upload user photo as a new avatar.")
  public UserProfilePage uploadUserAvatar(File file) throws FileNotFoundException {
    $x(uploadUserAvaterButtonLocator).uploadFile(file);
    return this;
  }

  @Step("Get the \"Uploaded User Avatar Message\".")
  public UserProfilePage getUploadedUserAvatarMessage(String message) {
    $x(uploadedUserAvatarMessageLocator).shouldHave(text(message));
    return this;
  }
}
