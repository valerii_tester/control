package com.testapp.ui.pageobject;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class LoginPage {

  private String loginFieldLocator = "//div[@class='foo']";
  private String passwordFieldLocator = "//div[@class='foo']";
  private String loginButtonLocator = "//div[@class='foo']";

  @Step("Type the user email: {email}.")
  public LoginPage enterEmail(String email) {
    $x(loginFieldLocator).setValue(email);
    return this;
  }

  @Step("Type the user password: {password}.")
  public LoginPage enterPassword(String password) {
    $x(passwordFieldLocator).setValue(password);
    return this;
  }

  @Step("Click the login button.")
  public HomePage clickLoginButton() {
    $x(loginButtonLocator).click();
    return page(HomePage.class);
  }
}
