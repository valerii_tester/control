package com.testapp.ui.pageobject;

import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class HomePage {

  private String openProfilButtonLocator = "//div[@class='foo']";

  @Step("Open the user profile page.")
  public UserProfilePage openProfilePage() {
    $x(openProfilButtonLocator).click();
    return page(UserProfilePage.class);
  }


}
